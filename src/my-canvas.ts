import { autoinject } from 'aurelia-framework'
import { Api } from 'api'

@autoinject
export class MyCanvas {
  items = []

  constructor(private api: Api) {}

  addItem() {
    this.items = this.api.getStuff()
  }
}
