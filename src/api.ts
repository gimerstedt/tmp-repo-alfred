export class Api {
  count = 0

  getStuff() {
    this.count++
    const ret = []

    for (let i = 0; i < this.count; i++)
      ret.push({
        a: i,
        b: `i=${i}`,
      })
    return ret
  }
}
